variable "region" {
  type    = string
  default = "eu-central-1"
}

variable "aws_rds_master_pass" {
  #sensitive = true
  type      = string
}

variable "ssh_public_key" {
  type = string
}

variable "application" {
  type    = string
  default = "mydrupal"
}

variable "environment" {
  type    = string
  default = "dev"
}

variable "argocd_admin_password" {
  #sensitive = true
  type      = string
}
