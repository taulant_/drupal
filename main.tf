provider "aws" {
  region = var.region
  default_tags {
    tags = merge({
      Application = title(var.application)
    }, local.common_tags)
  }
}

#${local.cidr} = 172.26 , 172.27, 172.28
module "vpc" {
  version = "5.9.0"
  source  = "terraform-aws-modules/vpc/aws"
  name    = "${var.application}-${var.environment}"
  cidr    = "${local.cidr}.0.0/17"

  azs             = data.aws_availability_zones.available.names
  private_subnets = ["${local.cidr}.1.0/24", "${local.cidr}.2.0/24", "${local.cidr}.3.0/24"]
  public_subnets  = ["${local.cidr}.101.0/24", "${local.cidr}.102.0/24", "${local.cidr}.103.0/24"]

  enable_nat_gateway = true
  single_nat_gateway = true
  #enable_vpn_gateway = true
}

module "self_signed_cert" {
  source = "./modules/selfsigncert"
}

module "eks" {
  source                         = "terraform-aws-modules/eks/aws"
  version                        = "20.29.0"
  cluster_name                   = "${var.application}-${var.environment}"
  cluster_version                = "1.31"
  cluster_endpoint_public_access = true

  # Cluster access entry
  # To add the current caller identity as an administrator
  enable_cluster_creator_admin_permissions = true

  vpc_id     = module.vpc.vpc_id
  subnet_ids = module.vpc.private_subnets
  eks_managed_node_groups = {
    blue = {
      min_size     = 1
      max_size     = 2
      desired_size = 2
    }
    green = {
      min_size     = 1
      max_size     = 2
      desired_size = 2

      instance_types = ["t3.large"]
      capacity_type  = "SPOT"
    }
  }

  tags = merge({
    Application = title(var.application)
  }, local.common_tags)
}

#Init K8S provider
resource "null_resource" "kubeconfig" {
  provisioner "local-exec" {
    command     = "aws eks update-kubeconfig --region ${var.region} --name ${module.eks.cluster_name}"
    interpreter = ["/bin/bash", "-c"]
  }
}

# 2. Step installing argocd and necessary app
module "base" {
  source           = "./modules/eks_base"
  region           = var.region
  cluster_name     = module.eks.cluster_name
  aws_account      = data.aws_caller_identity.current.account_id
  oidc_provider_id = module.eks.cluster_oidc_issuer_url
  subnet_ids       = module.vpc.public_subnets
  certificate_arn  = module.self_signed_cert.my_cert_arn
  domain           = "argocd.um.team"

  argocd_admin_password = var.argocd_admin_password
  helm_charts = {
    argocd                   = "7.3.11"
    nginx_ingress_controller = "11.5.3"
    alb_ingress_controller   = "1.9.2"
  }

  tags = merge({
    Application = title(var.application)
  }, local.common_tags)
}

#k8s setup default storageclasss
# resource "null_resource" "default_storageclass" {
#   provisioner "local-exec" {
#     command     = "kubectl patch storageclass gp2 -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'"
#     interpreter = ["/bin/bash", "-c"]
#   }
# }

# 3. Step installing app
module "drupal" {
  source                = "./modules/drupal"
  application           = "drupal-${var.environment}"
  namespace             = module.base.namespace
  environment           = var.environment
  helm_chart_version    = "20.0.12"
  domain                = "drupal.um.team"
  argocd_project        = "default"
  argocd_namespace      = module.base.namespace
  ingressClassName      = "nginx"
  userPassword          = var.aws_rds_master_pass
  argocd_domain         = "argocd.um.team"
  argocd_admin_password = var.argocd_admin_password
}

module "magento" {
  source                = "./modules/magento"
  application           = "magento-${var.environment}"
  namespace             = module.base.namespace
  environment           = var.environment
  helm_chart_version    = "28.0.4"
  domain                = "magento.um.team"
  argocd_project        = "default"
  argocd_namespace      = module.base.namespace
  ingressClassName      = "nginx"
  magentoPassword       = var.aws_rds_master_pass
  mariadb_password      = var.aws_rds_master_pass
  mariadb_root_password = var.aws_rds_master_pass
  argocd_domain         = "argocd.um.team"
  argocd_admin_password = var.argocd_admin_password
}

module "wp" {
  source                = "./modules/wordpress"
  application           = "wp-${var.environment}"
  namespace             = "wp-${var.environment}"
  environment           = var.environment
  helm_chart_version    = "23.1.18"
  domain                = "wordpress.um.team"
  argocd_project        = "default"
  argocd_namespace      = module.base.namespace
  ingressClassName      = "nginx"
  userPassword          = var.argocd_admin_password
  argocd_domain         = "argocd.um.team"
  argocd_admin_password = var.argocd_admin_password
}
