
# RSA key of size 4096 bits
resource "tls_private_key" "mykey" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "tls_self_signed_cert" "mycert" {
  private_key_pem = tls_private_key.mykey.private_key_pem

  subject {
    common_name  = "example.com"
    organization = "ACME Examples, Inc"
  }

  validity_period_hours = 8496

  allowed_uses = [
    "key_encipherment",
    "digital_signature",
    "server_auth",
  ]
}

resource "aws_acm_certificate" "cert" {
  private_key      = tls_private_key.mykey.private_key_pem
  certificate_body = tls_self_signed_cert.mycert.cert_pem
}

output "my_cert_arn" {
  value = aws_acm_certificate.cert.arn
}