provider "argocd" {
  server_addr = "${var.argocd_domain}:443"
  username    = "admin"
  password    = var.argocd_admin_password
  insecure    = "true"
}

resource "argocd_application" "app" {
  metadata {
    name      = var.application
    namespace = var.argocd_namespace
    labels = {
      environment = var.environment
      namespace   = var.argocd_namespace
    }
  }
  wait = true
  timeouts {
    create = "20m"
    delete = "10m"
  }
  spec {
    project = var.argocd_project
    source {
      repo_url        = "https://charts.bitnami.com/bitnami"
      chart           = "wordpress"
      target_revision = var.helm_chart_version
      helm {
        release_name = var.application

        parameter {
          name  = "ingress.enabled"
          value = true
        }

        parameter {
          name  = "service.type"
          value = "ClusterIP"
        }
        parameter {
          name  = "ingress.ingressClassName"
          value = var.ingressClassName
        }
        parameter {
          name  = "ingress.hostname"
          value = var.domain
        }
        parameter {
          name  = "wordpressPassword"
          value = var.userPassword
        }
      }
    }
    destination {
      server    = "https://kubernetes.default.svc"
      namespace = var.namespace
    }
    sync_policy {
      sync_options = ["CreateNamespace=true"]
      automated {
      }
      managed_namespace_metadata {
        labels = {
          "argocd.argoproj.io/managed-by" = var.argocd_namespace
        }
      }
    }
  }
  lifecycle {
    ignore_changes = [metadata]
  }
}
