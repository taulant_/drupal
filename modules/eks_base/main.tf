#NOTICE export vars
#export KUBE_CONFIG_PATH=~/.kube/config
#export KUBECONFIG=~/.kube/config

provider "kubernetes" {
  config_path = "~/.kube/config"
}

resource "kubernetes_namespace" "this" {
  metadata {
    name = var.cluster_name
  }

  lifecycle {
    ignore_changes = [metadata]
  }
}

module "ebs_csi_driver_controller" {
  source  = "DrFaust92/ebs-csi-driver/kubernetes"
  version = "3.10.0"

  ebs_csi_controller_role_name               = "ebs-csi-driver-controller"
  ebs_csi_controller_role_policy_name_prefix = "ebs-csi-driver-policy"
  oidc_url                                   = var.oidc_provider_id

}

####ALB Ingress Controller

resource "aws_iam_policy" "albcontriampolicy" {
  name = "AWSLoadBalancerControllerIAMPolicy${var.cluster_name}"

  policy = file("${path.module}/files/iam-policy.json")
}

resource "aws_iam_role" "albcontriamrole" {
  name               = "AmazonEKSLoadBalancerControllerRole${var.cluster_name}"
  assume_role_policy = templatefile("${path.module}/files/load-balancer-role-trust-policy.tpl", { oidc-provider-id = split("/", var.oidc_provider_id)[4], aws-region = var.region, aws-account = var.aws_account })
}

resource "aws_iam_role_policy_attachment" "attach" {
  role       = aws_iam_role.albcontriamrole.name
  policy_arn = aws_iam_policy.albcontriampolicy.arn
}

resource "kubernetes_manifest" "sa-alb-controller" {
  manifest = {
    "apiVersion" = "v1"
    "kind"       = "ServiceAccount"
    "metadata" = {
      "name"      = "aws-load-balancer-controller"
      "namespace" = "kube-system"
      "labels" = {
        "app.kubernetes.io/component" = "controller"
        "app.kubernetes.io/name"      = "aws-load-balancer-controller"
      }
      "annotations" = {
        "eks.amazonaws.com/role-arn" = "arn:aws:iam::${var.aws_account}:role/${aws_iam_role.albcontriamrole.name}"
      }
    }

  }
}

resource "helm_release" "alb_ingress_controller" {
  name       = "alb-ingress-controller"
  repository = "https://aws.github.io/eks-charts"
  chart      = "aws-load-balancer-controller"
  version    = var.helm_charts.alb_ingress_controller
  namespace  = "kube-system"

  set {
    name  = "clusterName"
    value = var.cluster_name
    type  = "string"
  }

  set {
    name  = "serviceAccount.create"
    value = "false"
  }

  set {
    name  = "serviceAccount.name"
    value = "aws-load-balancer-controller" #"${kubernetes_manifest.sa-alb-controller}" The SA needs to exists
    type  = "string"
  }

  #   set {
  #     name  = "image.repository"
  #     value = "602401143452.dkr.ecr.${var.region}.amazonaws.com/amazon/aws-load-balancer-controller"
  #     type  = "string"
  #   }
  depends_on = [
    kubernetes_manifest.sa-alb-controller
  ]

}

resource "helm_release" "nginx_ingress_controller" {
  name       = "nginx-ingress-controller"
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "nginx-ingress-controller"
  version    = var.helm_charts.nginx_ingress_controller
  namespace  = "kube-system"

  set {
    name  = "ingressClassResource.default"
    value = "true"
    type  = "string"
  }

  set {
    name  = "config.use-forwarded-headers"
    value = "true"
    type  = "string"
  }

  set {
    name  = "config.proxy-buffer-size"
    value = "16k"
    type  = "string"
  }

  set {
    name  = "service.type"
    value = "NodePort"
    type  = "string"
  }

  set {
    name  = "service.nodePorts.http"
    value = "30415"
    type  = "string"
  }

  set {
    name  = "service.nodePorts.https"
    value = "31429"
    type  = "string"
  }
}


resource "kubernetes_ingress_v1" "alb_ingress" {
  wait_for_load_balancer = true
  metadata {
    name      = "alb-ingress-connect-nginx"
    namespace = "kube-system"

    annotations = {
      "alb.ingress.kubernetes.io/certificate-arn"    = "${var.certificate_arn}"
      "alb.ingress.kubernetes.io/listen-ports"       = "[{\"HTTP\": 80}, {\"HTTPS\": 443}]"
      "alb.ingress.kubernetes.io/ssl-redirect"       = "443"
      "alb.ingress.kubernetes.io/healthcheck-path"   = "/healthz"
      "alb.ingress.kubernetes.io/scheme"             = "internet-facing"
      "alb.ingress.kubernetes.io/target-type"        = "ip"
      "alb.ingress.kubernetes.io/subnets"            = join(",", var.subnet_ids)
      "kubernetes.io/ingress.class"                  = "alb"
      "alb.ingress.kubernetes.io/load-balancer-name" = "${var.cluster_name}"
    }
  }

  spec {
    rule {
      http {
        path {
          backend {
            service {
              name = "nginx-ingress-controller"
              port {
                number = 80
              }
            }
          }
          path      = "/"
          path_type = "Prefix"
        }
      }
    }
  }

  depends_on = [
    helm_release.nginx_ingress_controller
  ]
}

output "alb_ingress_dns" {
  value = kubernetes_ingress_v1.alb_ingress.status.0.load_balancer.0.ingress.0.hostname
}


##Argocd

resource "null_resource" "password_admin_bcrypt" {
  triggers = {
    source = var.argocd_admin_password
    value  = bcrypt(var.argocd_admin_password)
  }

  lifecycle {
    ignore_changes = [triggers["value"]]
  }
}


resource "helm_release" "argocd" {
  name       = "argocd-${var.cluster_name}"
  repository = "https://argoproj.github.io/argo-helm"
  chart      = "argo-cd"
  version    = var.helm_charts.argocd
  #skip_crds  = true

  namespace = var.cluster_name

  set {
    name  = "server.ingress.enabled"
    value = true
  }

  set {
    name  = "global.domain"
    value = var.domain
  }

  values = [
    <<-EOT
    configs:
      params:
        server.insecure: true
    EOT
  ]

  set {
    name  = "dex.enabled"
    value = false
  }

  set_sensitive {
    name  = "configs.secret.argocdServerAdminPassword"
    value = null_resource.password_admin_bcrypt.triggers["value"]
  }

}