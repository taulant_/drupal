variable "region" {
  type = string
}

variable "oidc_provider_id" {
  type    = string
  default = ""
}

variable "domain" {
  type = string
}

variable "cluster_name" {
  type = string
}

variable "aws_account" {
  type = string
}

variable "subnet_ids" {
}

variable "argocd_admin_password" {
  type = string
}

variable "tags" {
  type = map(string)
}


variable "certificate_arn" {
  type = string
}

variable "helm_charts" {
}
