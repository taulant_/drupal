terraform {
  required_providers {
    argocd = {
      source  = "argoproj-labs/argocd"
      version = "~> 7.0.3"
    }
  }
}

provider "argocd" {
  server_addr = "${var.argocd_domain}:443"
  username    = "admin"
  password    = var.argocd_admin_password
  insecure    = "true"
}