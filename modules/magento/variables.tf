variable "application" {
  type = string
}

variable "namespace" {}

variable "environment" {}

variable "domain" {}

variable "helm_chart_version" {}

variable "magentoPassword" {
  #sensitive = true
}

variable "argocd_namespace" {}

variable "argocd_project" {}

variable "mariadb_password" {
  #sensitive = true
}

variable "mariadb_root_password" {
  #sensitive = true
}

variable "ingressClassName" {}

variable "argocd_admin_password" {
  type = string
}

variable "argocd_domain" {
  type = string
}
