variable "application" {
  type = string
}

variable "environment" {}

variable "namespace" {}

variable "ingressClassName" {}

variable "argocd_namespace" {}

variable "argocd_project" {}

variable "argocd_admin_password" {
  type = string
}

variable "domain" {
  type = string
}

variable "argocd_domain" {
  type = string
}

variable "helm_chart_version" {
  type = string
}

variable "userPassword" {
  #sensitive = true
}
