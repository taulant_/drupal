terraform {
  backend "s3" {
    bucket  = "umtao-project-cloud-tf-state"
    key     = "drupal/terraform.tfstate"
    region  = "us-east-1"
    encrypt = true
  }
}
