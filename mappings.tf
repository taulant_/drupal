locals {
  #Default tags for the infra portal
  common_tags = {
    Environment = upper(terraform.workspace)
    Repository  = basename(abspath("${path.root}"))
  }

  cidr = tomap({
    "dev"  = "172.26",
    "uat"  = "172.27",
    "prod" = "172.28"
  })[var.environment]

  install_plan = tomap({
    "dev"  = "small",
    "uat"  = "medium",
    "prod" = "large"
  })[var.environment]

  aws_instance_type = tomap({
    "dev" = {
      general  = "t3.medium"
      admin    = "t3.medium"
      master   = "t4g.micro"
      nfs      = "t4g.micro" #"t3.small"
      varnish  = "t4g.micro"
      cloudlog = "t4g.micro"
    }
    "uat" = {
      general  = "t4g.medium"
      admin    = "t4g.medium"
      master   = "t4g.micro"
      nfs      = "t4g.micro" #"t3.small"
      varnish  = "t4g.micro"
      cloudlog = "t4g.micro"
    }
    "prod" = {
      general  = "m6g.large"
      admin    = "m6g.large"
      master   = "t4g.micro"
      nfs      = "t4g.micro" #"t3.small"
      varnish  = "t4g.micro"
      cloudlog = "t4g.micro"
    }
  })[var.environment]
}